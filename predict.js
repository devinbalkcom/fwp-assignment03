
// Function to load the SP500 data, by hf.  You do not need to modify this
//  function.
var getSP500 = function() {
  var SP500 = [];
  $.ajax({
    url: 'SP500.txt',
    type: 'get',
    dataType: 'html',
    async: false,
    success: function(data) {
      SP500 = data.split('\n'); // each entry contains: open, high, low, close
      SP500.pop(); // delete last empty entry
    }
  });
  return SP500;
};

// Function to predict the value of the sp500 for each day over the range.
var predict = function() {

  var SP500 = getSP500();

  for( var i = SP500.length - 1; i > 0 ; i-- ) {
    var day = SP500[i].split(' ').map(Number);
    var open = day[1];
    var high = day[2];
    var low = day[3];
    var close = day[4];

    // add your prediction logic


  }
};
